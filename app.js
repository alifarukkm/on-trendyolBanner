var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var fs = require('fs');
var https = require('https');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var jsonRouter  = require('./routes/json');
const parser	= require('xml2json');

var app = express();
console.log(__dirname);
//Get trendyol data from api

var requestLoop = setInterval(function(){
  https.get('https://transport.productsup.io/1245f0c62b1d1a45701b/channel/88848/top250-media-buying.xml',(res) => {
	let data = '';

	res.on('data', (chunk) => {
		data +=chunk;
	});

	res.on('end', () => {
		var json = parser.toJson(data);
    var obj = JSON.parse(json);
		fs.writeFile(__dirname+'/public/javascripts/myjsonfile.json',json,'utf8');
  });
  res.on('socket', (s) => {
    s.setKeepAlive(true,120000);
  });
}).on("error",(err)=> {
	console.log("Error : " + err.message);
});
}, 60000);



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
//app.use('/json',jsonRouter);





// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
